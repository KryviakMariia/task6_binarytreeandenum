package com.kryviak.enums;

public enum Menu {

    START(1, "Print all"),
    SORT(2, "Sort menu"),
    SEARCH(3, "Search operation"),
    EXIT(0, "Exit");


    private final int id;
    private final String operation;

    Menu(int id, String operation ) {
        this.id = id;
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "Choose: [" + this.id + ", " + this.operation + "]";
    }
}
