package com.kryviak.binarytree;

public class MyBinaryTree {
    Node root;

    static class Node {
        int key;
        String value;
        Node left;
        Node right;
        Node parent;

        public Node(int key, String value, Node parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }
    }

    Node get(Node t, int key) {
        if (t == null || t.key == key)
            return t;
        if (key < t.key)
            return get(t.left, key);
        else
            return get(t.right, key);
    }

    public Node get(int key) {
        return get(root, key);
    }

    Node put(Node t, Node parent, int key, String value) {
        if (t == null) {
            t = new Node(key, value, parent);
        } else {
            if (key < t.key)
                t.left = put(t.left, t, key, value);
            else
                t.right = put(t.right, t, key, value);
        }
        return t;
    }

    public void put(int key, String value) {
        root = put(root, null, key, value);
    }

    void replace(Node a, Node b) {
        if (a.parent == null)
            root = b;
        else if (a == a.parent.left)
            a.parent.left = b;
        else
            a.parent.right = b;
        if (b != null)
            b.parent = a.parent;
    }

    void remove(Node t, int key) {
        if (t == null)
            return;
        if (key < t.key)
            remove(t.left, key);
        else if (key > t.key)
            remove(t.right, key);
        else if (t.left != null && t.right != null) {
            Node m = t.right;
            while (m.left != null)
                m = m.left;
            t.key = m.key;
            t.value = m.value;
            replace(m, m.right);
        } else if (t.left != null) {
            replace(t, t.left);
        } else if (t.right != null) {
            replace(t, t.right);
        } else {
            replace(t, null);
        }
    }

    public void remove(int key) {
        remove(root, key);
    }

    void print(Node t) {
        if (t != null) {
            print(t.left);
            System.out.println(t.key + "." + t.value);
            print(t.right);
        }
    }

    public void print() {
        print(root);
        System.out.println();
    }

    public static void main(String[] args) {
        MyBinaryTree tree = new MyBinaryTree();

        tree.put(3, "First");
        tree.put(2, "Second");
        tree.put(6, "Third");
        tree.put(3, "Fourth");
        tree.put(1, "Five");
        tree.put(4, "Last");
        tree.print();
        tree.remove(9);
        tree.print();
        tree.print(tree.get(4));
    }
}